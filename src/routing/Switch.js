import React from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import MainContainer from '../containers/MainContainer'
import HomeContainer from '../containers/HomeContainer'
import DashboardContainer from '../containers/DashboardContainer'
import EditEntryDetailContainer from '../containers/EditEntryDetailContainer'
import LoginContainer from '../containers/LoginContainer'
import RegisterContainer from '../containers/RegisterContainer'
import Page404 from '../components/Page404'


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
       localStorage.getItem('Bearer') !== '' ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const getRoutes = () => {
  return (
    <Router>
      <MainContainer>
          <Switch>
              <Route exact path="/" component={HomeContainer} />
              <Route exact path="/login" component={LoginContainer} />
              <Route exact path="/register" component={RegisterContainer} />
              <PrivateRoute exact path="/dashboard" component={DashboardContainer} />
              <PrivateRoute exact path="/entry/:id" component={EditEntryDetailContainer} />
              <PrivateRoute exact path="/entry/create" component={EditEntryDetailContainer} />
              <PrivateRoute exact path="/entry/edit/:id" component={EditEntryDetailContainer} />
              <Route component={Page404} />
          </Switch>
       </MainContainer>
    </Router>
  )
}

export default getRoutes