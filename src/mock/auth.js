const fakeAuth = {
    authenticate(cb) {
      localStorage.setItem('auth', true)
      setTimeout(cb, 100);
    },
    signout(cb) {
      localStorage.setItem('auth', false)
      setTimeout(cb, 100);
    }
};

export default fakeAuth