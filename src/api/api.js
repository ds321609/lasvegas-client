const base = process.env.REACT_APP_API_ADDRESS ? `${process.env.REACT_APP_API_ADDRESS}/api` : `http://localhost:8000/api`;

export const registerUser = async function(payload) {
    const options = {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
            "Content-Type": "application/json"
        }
    };
    try {
        const response = await fetch(`${base}/register`, options).then((res) => {
            return res.json()  
        });

        if(response.statusCode >= 400) {
            throw new Error(response.message)
        }

        localStorage.setItem('Bearer', response.token)

        return { isSuccess: true, contents: `${response.message}`}

    } catch(err) {
        return err
    }
    
}

export const authenticate = async function(payload) {
    const options = {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('Bearer')
        }
    };
    const response = await fetch(`${base}/login`,options).then((res) => {
        return res.json();
    }).catch((err) => {
        return err;
    });

    if(response.token) {
        localStorage.setItem('Bearer', response.token)
        return { isSuccess: true, token: response.token }
    } else {
        return { isSuccess: false, contents: 'The credentials that were entered are invalid'}
    }
    
    
}

export const getEntries = async function() {
    const options = { 
        headers:  {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('Bearer') 
            } 
        }
    const response = await fetch(`${base}/entries`, options).then((res) => {
      return res.json()
    }).catch((err) => {
        return err
    })
   
    return response;
}

export const getEntryById = async function(id) {
    const response = await fetch(`${base}/entries/${id}`,{ headers: { "Content-Type": "application/json",
    "Authorization": localStorage.getItem('Bearer') } }).then((res) => {
        if(res.status > 400) throw { isSuccess: false }
        return res.json()
        
    }).then((res) => {
        return {
            contents: res,
            isSuccess: true    
        }
        }).catch(err => {
            return {
                content: err.message,
                isSuccess: false
            }
    })

    return response
}

export const putEntry = async function(id, entry) {
    const options = {
        method: 'PUT',
        headers:  {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('Bearer') 
        },
        body: JSON.stringify(entry)
    }

    const response = await fetch(`${base}/entries/${id}`, options).then((res) => {
        return res.json()
    }).then((res) => {
        if(res.status > 400) return { isSuccess: false, contents: "An error occurred when submitting the entry. Please reach out to Support for further assistance" }

        return {
            contents: res,
            isSuccess: true
        }
    }).catch((err) => {
        return { contents: "An error occurred when submitting the entry. Please reach out to Support for further assistance", isSuccess: false }
    })

    return response
}

export const postEntry = async function(entry) {
    const options = {
        method: 'POST',
        headers:  {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('Bearer') 
        },
        body: JSON.stringify(entry)
    }

    const response = await fetch(`${base}/entries`, options).then((res) => {
        return res.json()
    }).then((res) => {
        if(res.status > 400) return { isSuccess: false, contents: "An error occurred when submitting the entry. Please reach out to Support for further assistance" }

        return {
            contents: res,
            isSuccess: true
        }
    }).catch((err) => {
        return { contents: "An error occurred when submitting the entry. Please reach out to Support for further assistance", isSuccess: false }
    })

    return response
}

export const deleteEntry = async function(id) {
    const options = {
        method: 'DELETE',
        headers:  {
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('Bearer') 
        }
    }

    const response = await fetch(`${base}/entries/${id}`, options).then((res) => {

        if(res.status > 400) return { isSuccess: false, contents: "An error occurred when submitting the entry. Please reach out to Support for further assistance" }
        return {
            contents: res,
            isSuccess: true
        }
    }).catch((err) => {
        return {
            contents: err.message,
            isSuccess: false
        }
    })
}