export const attributes = [
    {
        id: 'looks',
        name: 'looks',
        description: 'Looks  are  actually  not  about  your  physical  features.  They  are  about how you present yourself. Rate yourself on your grooming, posture, eye contact, whether you stand out in a positive way, and if your style attracts the type of person you want to be with'
    },
    {
        id: 'adaptability',
        name: 'adaptability',
        description: 'Ever notice that uptight men tend not to do well with women? This is because they aren’t adaptable. Rate yourself on your adventurousness, spontaneity, independence, risk-taking, social intelligence, flexibility, and ability to handle new situations and environments.'
    },
    {
        id: 'strength',
        name: 'strength',
        description: 'Strength is the ability to protect people and make them feel safe. Some men  display  this  through  money  or  muscle,  but  those  aren’t  necessary—and often aren’t enough. So rate yourself on being an effective communicator, having a powerful frame, living in your own reality, your ability to take care of others, and criteria such as assertiveness, leadership ability, courage, loyalty, decisiveness, and self-assurance. '
    },
    {
        id: 'value',
        name: 'value',
        description: 'Value  actually  consists  of  three  elements:  what  you  think  your  value is, what she thinks it is, and what impartial observers think it is. Rate yourself on the degree to which you’re the leader of a social circle, admired by others, able to teach people things, and comfortable displaying  high-status  behaviors.  Other  criteria  include  being  intelligent,  interesting,  talented,  entertaining,  successful,  self-sufficient, and creative. '
    },
    {
        id: 'emotionConn',
        name: 'emotional connection',
        description: 'This is the home of rapport and abstract concepts like chemistry. It’s about possessing qualities that make people feel excited, connected,comfortable, and understood around you, as if they’ve just met a best friend  or  soulmate.Rate  yourself  on  your  success in finding  commonalities  with  strangers, creating  deep  rapport  with  people, being  in touch  with  your  feelings, and  listening  closely  to  others, and on criteria such as compassion, positivity, selflessness, and empathy.'
    },
    {
        id: 'goals',
        name: 'goals',
        description: 'Goals  are  defined  not  by  what  you  do,  but  by  your  ambitions and what  you’re  capable  of  doing.  Rate yourself on the clarity of your goals, dreams, and hunger for life. You can measure your potential to achieve  them  by  determining  if  you  possess  traits  like  stability,  efficiency, perseverance, and the ability to learn quickly.'
    },
    {
        id: 'authenticity',
        name: 'authenticity',
        description: 'An  authentic  person  is  happy  with  himself  and  embraces  even  his imperfections.  Rate  yourself  on  your  “congruence”—the  alignment between  the  face  you  show  to  the  world  and  what  you’re  really  like on the inside. Keep in mind that having contradictory sides to your personality doesn’t make you incongruent. Having a duality, contradiction, or complications can make you more rich and compelling as a person. But being phony, insincere, or disingenuous do not.'
    },
    {
        id: 'selfWorth',
        name: 'self-worth',
        description: 'This may be the single most important attribute here, and the well-spring from which most of the others flow. Rate yourself on your sense of confidence and worthiness, as well as your lack of fears and insecurities about yourself. Examine your willingness to take up space as you move through the world, how well you accept compliments, how comfortable you are when other people pay attention to you, and how much you deserve the devotion of a woman of the highest caliber. Do you truly believe that you’re entitled to the best the world has to offer?'
    }
];