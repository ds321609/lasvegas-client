import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import getRoutes from './routing/Switch'

ReactDOM.render(
    getRoutes(), 
    document.getElementById('root'));
registerServiceWorker();
