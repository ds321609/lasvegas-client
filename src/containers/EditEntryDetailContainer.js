import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import EditEntry from '../components/EditEntry'
import { getEntryById, postEntry, putEntry, deleteEntry } from '../api/api'
import { Header, Button } from 'semantic-ui-react'

export default class EditEntryDetailContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      isEditable: true,
      isNew: true,
      isSuccess: false,
      errors: {},
      date: Math.round(Date.now() / 1000),
      id: this.props.match.params.id,
      activeIndex: -1,
      attributes: {
        looks: 0,
        adaptability: 0,
        strength: 0,
        value: 0,
        emotionConn: 0,
        goals: 0,
        authenticity: 0,
        selfWorth: 0
      }
    }
  }

  componentWillMount() {
    const isEditable = /edit/g.test(this.props.match.url)
    const isNew = /create/g.test(this.props.match.url)  

    this.setState({ isEditable, isNew })
  }

  toggleEdit() {
    this.setState({ isEditable: true })
  }

  async componentDidMount() {
    const id = this.props.match.params.id
    let entry;

    if(!this.state.isNew) {
      entry = await getEntryById(id)
    } else {
      return;
    }

    if(!entry.isSuccess) {
      this.setState({ errors: { fetch: 'This entry doesn\'t exist anymore. Please try another entry.'}})
      return;
    } 
    const attributes = entry.contents.entry[0].attributes
  
    this.setState({ attributes: JSON.parse(JSON.stringify(attributes))})
  }

  async handleEntry() {

    let response
    let errors = {}
    const attributes = () => {
      let state = this.state.attributes
      for(var props in state) {
        state[props] = parseInt(state[props], 10)
      }

      return state
    }
    const score = Object.values(attributes()).reduce((acc, current) => {
      return acc + current
    }) / 8

    const update = !this.state.isNew

    const entry = {
      date: this.state.date,
      attributes: attributes(),
      score: parseFloat(score.toFixed(1))
    }
    
    for(var props in entry.attributes) {
      const input = parseInt(entry.attributes[props], 10)

      if(isNaN(input)) {
        errors[props] = 'Must be a valid number'
        entry.attributes[props] = 0
      } else if(input > 10) {
        errors[props] = 'Score must be between 0 and 10'
      } else if(input < 0) {
        errors[props] = 'Score must be between 0 and 10'
      }
    }

    if(Object.keys(errors).length > 0) {
      this.setState({ errors, attributes: entry.attributes })
    } else if(!update) {
      this.setState({ errors: {} })
      response = await postEntry(entry)
      response.isSuccess ? this.setState({ isSuccess: response.isSuccess , isEditable: false, isNew: false }) : this.setState({isSuccess: response.isSuccess, isEditable: false, isNew: false, errors: { api: response.contents }})
    } else {
      this.setState({ errors: {} })
      response = await putEntry(this.props.match.params.id, entry)
      response.isSuccess ? this.setState({ isSuccess: response.isSuccess , isEditable: false, isNew: false }) : this.setState({isSuccess: response.isSuccess, isEditable: false, isNew: false, errors: { api: response.contents }})
    }

  }

  cancelEdit() {
    this.setState({ isEditable: false, errors: {} })
  }

  async removeEntry(id) {
    await deleteEntry(id)
    this.props.history.push('/dashboard')

  }

  handleChange(e) {
    let attributes = this.state.attributes
    const target = e.target.name
    const value = e.target.value

    attributes[target] = value
    
    this.setState({ attributes })
  }

  handleAccordion(e, titleProps) {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  render() {
    return (
      <div>
        {this.state.isEditable || this.state.isNew ? null : <Button onClick={this.toggleEdit.bind(this)} primary fluid >Edit Entry</Button>}
        {this.state.errors.fetch ? <Header as="h3" className="ui center aligned" color="red">{this.state.errors.fetch}<div><Link to="/dashboard">Go back to Dashboard</Link></div></Header> : 
          <div>
            <EditEntry match={this.props.match} 
                      state={this.state} 
                      handeChange={this.handleChange.bind(this)} 
                      handleEntry={this.handleEntry.bind(this)}
                      toggleEdit={this.toggleEdit.bind(this)}
                      removeEntry={this.removeEntry.bind(this)}
                      cancelEdit={this.cancelEdit.bind(this)}
                      handleAccordion={this.handleAccordion.bind(this)} />         
          </div>
                    }
        {this.state.errors.api ? <Header as="h3" className="ui center aligned" color="red">{this.state.errors.api}</Header> : null}
        {this.state.isSuccess && this.state.isNew ? <Header as="h3" color="green">Success! The entry was successfully added to your journal! Click <Link to="/dashboard">here</Link> to go back your dashboard</Header> : null }
        {this.state.isSuccess && !this.state.isNew ? <Header as="h3" color="green">Success! The entry was successfully updated! Click <Link to="/dashboard">here</Link> to go back your dashboard</Header> : null }
      </div>
    )
  }
}
