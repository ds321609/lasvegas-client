import React from 'react'
import { Container } from 'semantic-ui-react'
import NavigationContainer from './NavigationContainer'
import FooterContainer from './FooterContainer'

import '../App.css';
import 'semantic-ui-css/semantic.min.css';

export default (props) => {
  return (
    <div>
      <div className="wrapper">
      <NavigationContainer />
      <Container style={ { paddingBottom: '20vh' } }>
          {props.children}
      </Container>
      </div>  
      <FooterContainer />
    </div>
  )
}
