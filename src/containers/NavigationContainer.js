import React from 'react'
import { Link } from 'react-router-dom'
import { Menu, Container } from 'semantic-ui-react'
import AuthButton from '../components/AuthButton'

export default () => {
        const auth = localStorage.getItem('Bearer') !== '' ? true : false;
        const anonUser = [
                { key: 'home', as: Link, to:"/", name: 'Home' },
                { key: 'register', as: Link, to:"/register", name: 'Register' },
        ];
        const authUser = [
                { key: 'home', as: Link, to:"/", name: 'Home' },
                { key: 'dashboard', as: Link, to:"/dashboard", name: 'Dashboard' }
        ]
        const populateLinks = (links) => {
                return links.map((link, idx) => {
                        return <Menu.Item key={idx} name={link.name} as={link.as} to={link.to} />
                })
        }

        return (
        <Menu inverted size="large">
                <Container>
                        {
                                auth ? populateLinks(authUser) : populateLinks(anonUser)
                        }
                <Menu.Menu position="right"><AuthButton /></Menu.Menu>
                </Container>
        </Menu>
        )
}


