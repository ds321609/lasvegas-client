import React from 'react'
import { Container } from 'semantic-ui-react'

export default () => {
  return (
    <div className="footer">
      <Container className="footer-container">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aut adipisci saepe voluptatem rem natus distinctio temporibus quidem, optio corporis nemo explicabo quia excepturi qui asperiores quibusdam nihil amet, accusamus architecto.
      </Container>
    </div>
  )
}
