import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Register from '../components/Register'
import Success from '../components/Success'
import { Loader } from 'semantic-ui-react'
import { registerUser } from '../api/api'

export default class RegisterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email: '',
        password: '',
        confirmPassword: '',
        errors: {},
        isLoading: false,
        isSuccess: false,
        redirectToReferrer: false
    }
  }  

  handleChange(e) {
    let state = this.state;
    const target = e.target.id;
    const value = e.target.value;

    state[target] = value;
    this.setState(state)
  }

  async handleRegister() {
    let response;
    let errors = {};
    const state = this.state;
    const emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/g;
    const passwordRegex = /^[a-zA-Z]\w{7,}$/g;

    for(var props in state) {
        switch(props) {
            case 'email': 
                if(!state[props].match(emailRegex)) {
                    errors.email = 'The email entered is invalid'
                }
                break;
            case 'password':
                if(!state[props].match(passwordRegex)) {
                    errors.password = 'The password must start with a letter. Can contain numbers and special characters (_, !). Must be at least 8 characters or more'
                }
                break;
            case 'confirmPassword':
                if(state.password !== state.confirmPassword) {
                    errors.confirmPassword = 'The passwords do not match'
                }
                break;
            default:
                break;
        }
    }

    if(Object.keys(errors).length !== 0) {
        this.setState({ errors })
        return;
    }
    
    this.setState({ isLoading: true })

    response = await registerUser({ email: state.email, password: state.password })

    this.setState({ isLoading: false })

    if(response instanceof Error) {
        errors.duplicate = response.message;
        this.setState({ errors })
        return;
    } else {
        this.setState({ isSuccess: response.isSuccess })
    }
    
    
  }

  redirectToDashboard() {
      this.setState({ redirectToReferrer: true })
  }

  render() {
    const { isLoading, isSuccess, errors } = this.state;
    const { from } = this.props.location.state || { from: { pathname: "/dashboard"} };
    const { redirectToReferrer } = this.state;

    if(redirectToReferrer) {
        return <Redirect to={from} />
    }


    return (
      <div>
        { isSuccess ? <Success redirectToDashboard={this.redirectToDashboard.bind(this)} /> : null}
        { !isSuccess ? <Register
                                        errors={errors} 
                                        isSuccess={isSuccess} 
                                        isLoading={isLoading}
                                        handleChange={this.handleChange.bind(this)} 
                                        handleRegister={this.handleRegister.bind(this)}/> : null}
      </div>
    )
  }
}
