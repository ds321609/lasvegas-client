import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { authenticate } from '../api/api'
import { Button, Segment, Input, Grid, Message, Responsive, Container } from 'semantic-ui-react'

export default class LoginContainer extends Component {
  constructor() {
    super()
    this.state = {
        redirectToReferrer: false,
        email: '',
        password: '',
        errors: {},
        isLoading: false
    }
  }

  async handleLogin() {
    const errors = {};
    const {email, password} = this.state;

    this.setState({ isLoading: true })

    const response = await authenticate({ email, password });

     
    if(!response.isSuccess) {
      errors.auth = response.contents
      this.setState({ errors, isLoading: false })
      return;
    }

    if(response.token) this.setState({ redirectToReferrer: true });
    
    this.setState({ isLoading: false })
    
  }

  handleChange(e) {
    let state = this.state
    const target = e.target.id;
    const value = e.target.value;

    state[target] = value;
    this.setState(state)
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/dashboard"} };
    const { redirectToReferrer, isLoading } = this.state;
    const errors = this.state.errors;

    if(redirectToReferrer) {
        return <Redirect to={from} />
    }

    return (
      <Container text={true}>
      <Responsive minWidth={780} >
        <Segment padded="very">
                  {errors.auth ? <Message negative header={errors.auth} />  : null }
                  <Input 
                    label={{ basic: true, content: 'Email'}}
                    labelPosition="left"
                    size="large"
                    fluid
                    id="email"
                    onChange={this.handleChange.bind(this)}
                    />
                  <Input 
                    label={{ basic: true, content: 'Password'}}
                    labelPosition="left"
                    size="large"
                    type="password"
                    fluid
                    id="password"
                    onChange={this.handleChange.bind(this)}
                    />
          </Segment>
      </Responsive>
      <Responsive maxWidth={780} >
        <Segment padded="very"> 
                  {errors.auth ? <Message negative content={errors.auth} />  : null }
                  <Input 
                  placeholder="Email"
                    fluid
                    id="email"
                    onChange={this.handleChange.bind(this)}
                    />
                  <Input 
                  placeholder="Password"
                    type="password"
                    fluid
                    id="password"
                    onChange={this.handleChange.bind(this)}
                    />
          </Segment>
      </Responsive>
        <Button size="large" color="green" loading={isLoading} onClick={this.handleLogin.bind(this)} floated="right" style={{ marginTop: 20 }}>Log in</Button>

      </Container>
    )
  }
}
