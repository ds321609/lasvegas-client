import React, { Component } from 'react'
import Dashboard from '../components/Dashboard'
import { Redirect } from 'react-router-dom';
import { Loader } from 'semantic-ui-react'
import { getEntries } from '../api/api'

export default class DashboardContainer extends Component {

  constructor() {
    super();
    this.state = {
      entries: [],
      isLoading: true,
      errors: {}
    }
  }

  async componentDidMount() {
    let errors = {};
    const entries = await getEntries();

    if(entries.isSuccess) this.setState({ entries: entries.entries, isLoading: false });

    if(entries.statusCode > 400) Object.assign(errors, { auth: true })

    this.setState({ errors });

  }

  render() {
    const entries = this.state.entries
    
    return (  
      <div>
          {this.state.errors.auth ? <Redirect to="/login" /> : null }
          {this.state.isLoading ? <Loader active inline="centered" size="large" >Working...</Loader> : <Dashboard entries={entries}/>}
      </div>
    )
  }
}
