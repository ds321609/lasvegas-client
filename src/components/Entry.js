import React from 'react'
import { Segment, Divider, Grid } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export default (props) => {
  const date = new Date(props.entry.date*1000).toLocaleDateString({ month: 'short', day: 'numeric'})
  const score = props.entry.score
  const id = props.entry._id

  return (
    <div>
      <Segment raised>
          <Link to={`entry/${id}`} style={ { 
        color: 'rgba(0,0,0,.87)',
        cursor: 'default'
          }}>
          <Grid className="ui center aligned" columns={2} relaxed>
            <Grid.Column>
                <Segment basic>
                    <div><strong>DATE:</strong></div>
                    <div>{date}</div>
                </Segment>
            </Grid.Column>
            <Divider vertical ></Divider>
            <Grid.Column>
                <Segment basic>
                    <div><strong>SCORE:</strong></div>
                    <div>{score}</div>
                </Segment>
            </Grid.Column>
          </ Grid>
          </Link>
        </Segment>
    </div>
  )
}
