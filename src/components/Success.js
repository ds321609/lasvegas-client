import React from 'react'
import { Segment, Button, Container, Header } from 'semantic-ui-react'

export default (props) => {
  const { redirectToDashboard } = props; 
  console.log(redirectToDashboard)
  return (
    <Segment>
      <Container>
        <Segment basic>
          <Header as="h2">
            Congratulations!
            <Header.Subheader>you have successfully registered your account! Click below to be taken to your dashboard</Header.Subheader>
          </Header>
          <Button primary size="large" onClick={redirectToDashboard}>Go to Dashboard</Button>
        </Segment>
      </Container> 
    </Segment>
  )
}
