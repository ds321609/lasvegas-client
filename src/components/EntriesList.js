import React from 'react'
import Entry from './Entry'
import { Segment, Header } from 'semantic-ui-react'

export default (props) => {
  const entries = props.entries;
  return (
    <div>
      { entries.length > 0 ? 
        entries.map((entry, idx) => {
          return <Entry key={idx} entry={entry} />
        }) : <Segment padded='very'>
                <Header>No entries? Create your first entry today!</Header>
             </Segment>
      }
      
    </div>
  )
}
