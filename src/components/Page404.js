import React from 'react'
import { Header, Segment } from 'semantic-ui-react'

export default () => {
  return (
    <div>
      <Header as="h1">Well, This is annoying...</Header>
      <Segment>This page you're looking for does not exist</Segment>
    </div>
  )
}
