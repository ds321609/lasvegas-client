import React from 'react'
import { attributes as attConstant } from '../api/attributes'
import { Input, Button, Label, Table, Responsive, Accordion, Icon, Transition } from 'semantic-ui-react'

export default (props) => {
  const attValues = props.state.attributes
  const editState = props.state.isEditable
  const errors = props.state.errors
  const isNew = props.state.isNew
  const id = props.state.id
  const activeIndex = props.state.activeIndex

  return (
    <div>
      <Table striped fixed padded>
          <Table.Body>
            {
              attConstant.map((att, idx) => {
                return (
                  <Table.Row key={idx}>
                    <Table.Cell>
                      <Responsive as='div' minWidth={780}>
                        <strong>{att.name.toUpperCase()}</strong>
                        <p>{att.description}</p>
                      </Responsive>
                      <Responsive as={Accordion} maxWidth={780} >
                        <Accordion.Title active={activeIndex === idx} index={idx} onClick={props.handleAccordion}>
                          <strong>{att.name.toUpperCase()}</strong>
                          <Icon name="dropdown" />
                        </Accordion.Title>
                        <Transition visible={activeIndex === idx} animation='fade down' duration={500}>
                          <Accordion.Content active={activeIndex === idx}>
                            {att.description}
                          </Accordion.Content>
                        </Transition>
                      </Responsive>
                    </Table.Cell>
                    <Table.Cell textAlign="center">
                    {editState || isNew ? <Input onChange={props.handeChange} name={att.id} value={attValues[att.id]}/> : attValues[att.id]}
                    {errors[att.id] ? <Label color='red' 
                                        style={ { display: 'block', position: 'absolute', right: '193px'} } pointing>{errors[att.id]}</Label> : null}
                    </Table.Cell>
                  </Table.Row>
                )
              })
            }
          </Table.Body>
      </Table>
    { editState || isNew ?
          <div> 
            <Button positive size="large" floated="right" onClick={() => props.handleEntry()}>Submit</Button>
            { !isNew ? <Button primary size="large" floated="right" onClick={() => props.cancelEdit()}>Cancel</Button> : null }
          </div>
           : null }
    { !editState && !isNew ? <Button negative floated="right" size="large" onClick={() => props.removeEntry(id) } >Delete</Button> : null} 
    </div>
  )
}
