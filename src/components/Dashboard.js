import React from 'react'
import EntriesList from './EntriesList'
import { Link } from 'react-router-dom'
import { Button } from 'semantic-ui-react'

export default (props) => {
  return (
    <div>
        <Button as={ Link } to="/entry/create" fluid primary>Add Entry</Button>
        <EntriesList entries={props.entries} />
    </div>
  )
}
