import React from 'react'
import { Button, Segment, Input, Message, Container, Responsive } from 'semantic-ui-react'

export default (props) => {
    const { handleChange, handleRegister, errors, isLoading } = props;
    const errorsList = Object.keys(errors).map((key, idx) => {
        return <Message.Item>{errors[key]}</Message.Item>      
    })

    return (
        <Container text>
                {Object.keys(errors).length > 0 ? <Message negative>
                <Message.Header floating="true">Please correct the errors listed below</Message.Header>
                <Message.List>
                    { errorsList }
                </Message.List>
                </Message> : null }
                    <Responsive minWidth={780}>
                        <Segment padded='very'>
                            <Input 
                                label={{ basic: true, content: 'Email'}}
                                labelPosition="left"
                                size="large"
                                fluid
                                id="email"
                                onChange={handleChange}
                                />
                            <Input 
                                label={{ basic: true, content: 'Password'}}
                                labelPosition="left"
                                size="large"
                                type="password"
                                fluid
                                id="password"
                                onChange={handleChange}
                                />
                                <Input 
                                label={{ basic: true, content: 'Confirm Password'}}
                                labelPosition="left"
                                size="large"
                                type="password"
                                fluid
                                id="confirmPassword"
                                onChange={handleChange}
                                />
                        </Segment>
                    </Responsive>
                    <Responsive maxWidth={780}>
                        <Segment padded='very'>
                            <Input 
                                placeholder="Email"
                                fluid
                                id="email"
                                onChange={handleChange}
                                />
                            <Input 
                                placeholder="Password"
                                type="password"
                                fluid
                                id="password"
                                onChange={handleChange}
                                />
                            <Input 
                                placeholder="Confirm password"
                                type="password"
                                fluid
                                id="confirmPassword"
                                onChange={handleChange}
                                />
                        </Segment>
                    </Responsive>
                <Button size="large" color="green" loading={isLoading} onClick={handleRegister} floated="right" style={{ marginTop: 20 }}>Register</Button>
        </Container>
    )
}
