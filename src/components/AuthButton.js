import React from 'react'
import { withRouter, Link } from 'react-router-dom'
import { Button, Menu } from 'semantic-ui-react'

const AuthButton = ({ history }) => {
  const auth = localStorage.getItem('Bearer')
  const signout = (cb) => {
    localStorage.setItem('Bearer', '')
    cb()
  }
  return (
    <Menu.Menu position="right">
        {auth !== '' ? (<Menu.Item floated="true" primary="true" onClick={() => signout(() => history.push("/"))} >Sign Out</Menu.Item>) :
        (<Menu.Item ><Button positive as={Link} to="/login">Login</Button></Menu.Item>)}
    </Menu.Menu>
  )
}

export default withRouter(AuthButton)
